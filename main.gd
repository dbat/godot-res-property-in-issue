@tool
extends Node2D

@export var tog:bool:
	set(b):
		test()

var r = load("res://suzanne.res")

func test():
	print("random_string" in r) # Why does this print true?

	# Some more sleuthing:
	#$ grep random_string suzanne.res
	# <no output>
	#$ strings suzanne.res | grep random_string
	# <no output>
